import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

interface Result {
  id: number,
  slotValues: string[],  
}
interface State {
  stateflg: boolean,
  result: Result[],
  modalflg: boolean,
  nextId: number
}
const state: State = {
  stateflg: false,
  result: [
    // {
    //   id: 0,
    //   slotValues: ["1", "5", "8"]
    // },
  ],
  modalflg: false,
  nextId: 0
}

export default new Vuex.Store<State>({
  // 状態管理
  state,

  getters: {
    getStateflg (state) {
      return state.stateflg
    },

    getResult (state) {
      return state.result
    },

    getModalflg (state) {
      return state.modalflg
    }
  },

  // 状態の更新
  mutations: {
    chgStateflg (state, {stateflg}) {
      state.stateflg = stateflg
    },

    addResult (state, {result}) {
      state.result.push({
        id: state.nextId,
        slotValues: result
      })

      // 次の追加に備えてIDを更新
      state.nextId++
    },

    chgModalflg (state, {modalflg}) {
      state.modalflg = modalflg
    }
  },

  // 非同期処理、使えば使う
  actions: {
  }
})